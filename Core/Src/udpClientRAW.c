#include "lwip/pbuf.h"
#include "lwip/udp.h"
#include "lwip/tcp.h"

#include "stdio.h"
#include "string.h"

#include "udpClientRAW.h"


void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);
static void udpClient_send(void);

struct udp_pcb *upcb;
char buffer[100];
int counter = 0;

extern TIM_HandleTypeDef htim1;

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	udpClient_send();
}

void udpClient_connect(void) {
	err_t err;

	/* 1.Create a new UDP control block */
	upcb = udp_new();

	/* Bind the block to module's IP and port */
	ip_addr_t myIPaddr;
	IP_ADDR4(&myIPaddr, 192, 168, 0, 111);
	udp_bind(upcb, &myIPaddr, 8);

	/* Configure destination IP and port */
	ip_addr_t DestIPaddr;
	IP_ADDR4(&DestIPaddr, 169, 254, 139, 61);
	err = udp_connect(upcb, &DestIPaddr, 7);

	if (err == ERR_OK) {
		udpClient_send();

		udp_recv(upcb, udp_receive_callback, NULL);
	}
}

static void udpClient_send(void) {
	struct pbuf *txBuf;
	char data[100];

	int len = sprintf(data, "sending UDP client message %d", counter);

	/* Allocate buf from pool */
	txBuf = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_RAM);

	if (txBuf != NULL) {
		/* copy data to pbuf */
		pbuf_take(txBuf, data, len);

		/* send udp data */
		udp_send(upcb, txBuf);

		/* free pbuf */
		pbuf_free(txBuf);
	}
}

void udp_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
	/* Copy the data from the pbuf */
	strncpy(buffer, (char*) p->payload, p->len);

	/* increment message count */
	counter++;

	/* Free receive pbuf */
	pbuf_free(p);
}

